package com.example.minhdang.retrofitwithrecycleviewandmoviedb;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Minh Dang on 10/6/2016.
 */
public interface MovieAdapterArray {
//    @GET("api_key=5446ca07c8dacacf459775023ac5c116")
//    Call<List<Movie>> getMovieDetails();
    String BASE_URL ="http://api.themoviedb.org/3/";
    @GET("movie/top_rated")
    Call<MovieRes> getTopRatedMovies(@Query("api_key") String apiKey);
}
