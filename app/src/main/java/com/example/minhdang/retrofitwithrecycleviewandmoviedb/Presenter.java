package com.example.minhdang.retrofitwithrecycleviewandmoviedb;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Minh Dang on 10/6/2016.
 */
public class Presenter extends AppCompatActivity {
    private Context context;
    RecyclerView recyclerView;
    String url = "http://api.themoviedb.org/3/";
    String apikey = "5446ca07c8dacacf459775023ac5c116";
    private static final String TAG = Presenter.class.getSimpleName();
    public Presenter(Context context, RecyclerView recyclerViewC) {
        this.context = context;
        recyclerView = recyclerViewC;
    }
    public void getData(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        MovieAdapterArray service = retrofit.create(MovieAdapterArray.class);
        service.getTopRatedMovies(apikey).enqueue(new Callback<MovieRes>() {
            @Override
            public void onResponse(Response<MovieRes> response, Retrofit retrofit) {
                List<Movie> movies = response.body().getResults();
                Log.d(TAG,"Count Movie:"+ movies.size());
                recyclerView.setAdapter(new MovieAdapter(movies, R.layout.main_menu, context));
            }
            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
