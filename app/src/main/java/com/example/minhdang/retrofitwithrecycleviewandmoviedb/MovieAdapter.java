package com.example.minhdang.retrofitwithrecycleviewandmoviedb;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Minh Dang on 10/6/2016.
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MoviewViewHolder> {
    List<Movie>  movies = new ArrayList<>();
    private int rowlayout;
    private Context context;
    public static class MoviewViewHolder extends RecyclerView.ViewHolder{
        LinearLayout moviesLayout;
        TextView movieTitle;
        TextView data;
        TextView movieDescription;
        TextView rating;
        public MoviewViewHolder(View v) {
            super(v);
            moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
            movieTitle = (TextView) v.findViewById(R.id.title);
            data = (TextView) v.findViewById(R.id.subtitle);
            movieDescription = (TextView) v.findViewById(R.id.description);
            rating = (TextView) v.findViewById(R.id.rating);
        }
    }

    public MovieAdapter(List<Movie> movies,int rowlayout,Context context){
        this.movies = movies;
        this.rowlayout = rowlayout;
        this.context = context;
    }
    @Override
    public MovieAdapter.MoviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowlayout,parent,false);
        return new MoviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MoviewViewHolder holder, int position) {
        holder.movieTitle.setText(movies.get(position).getTitle());
        holder.data.setText(movies.get(position).getRelease_date());
        holder.movieDescription.setText(movies.get(position).getOverview());
        holder.rating.setText((movies.get(position).getVote_average()));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
