package com.example.minhdang.retrofitwithrecycleviewandmoviedb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvMovie);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Presenter presenter = new Presenter(this,recyclerView);
        presenter.getData();
    }
}
